#! /usr/bin/env node


// Require modules
var http = require("http"),
	io = require("socket.io"),
	$ = require("./common.js");


/**
 * Pong5 server
 * @type {Object}
 */
var Pong5 = {
	"io": null,
	"http": null,

	"port": 1234,
	"dns": "192.168.0.21",

	"environments": {
		"production": {
			"minify": true,
			"etag": true,
			"gzip": true,
			"log": 1,
			"transports": ["websocket", "xhr-polling"]
		},
		"development": {
			"minify": false,
			"etag": false,
			"gzip": false,
			"log": 4,
			"transports": ['websocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']
		}
	},

	"clients": {},




	"init": function() {
		var self = this;

		$.log("debug", "Initializing Pong5 server");

		// Create http server
		this.http = http.createServer();
		this.http.listen(this.port, this.dns);

		// Initialize Socket.io
		this.io = io.listen(this.http);

		// Configure environment
		this.configure();

		// Listen to client connections
		this.io.sockets.on("connection", function(client) {
			self.register(client);
		});
	},

	"configure": function() {
		$.log("debug", "Configuring environments");

		// Apply Socket.IO configuration environments
		for (var env in this.environments) {
			this.configure_io(env);
		}
	},

	"configure_io": function(env) {
		var self = this;

		$.log("debug", "Configuring " + env + " environment");

		this.io.configure(env, function() {
			if (self.environments[env].minify) {
				self.io.enable('browser client minification');
			}
			if (self.environments[env].etag) {
				self.io.enable('browser client etag');
			}
			if (self.environments[env].gzip) {
				self.io.enable('browser client gzip');
			}
			self.io.set('log level', self.environments[env].log);
			self.io.set('transports', self.environments[env].transports);
		});
	},

	"register": function(client) {
		$.log("debug", "Client connected");

		this.clients[client.id] = new Pong5.Client(this, this.io, client);
	}
};





/**
 * Pong5 client
 * 
 * @param {Object} parent			Main pong5 server instance
 * @param {Socket.io} io			Global socket.io object
 * @param {Socket.io.client} client	Socket.io client connection instance
 */
Pong5.Client = function Client(parent, io, client) {
	$.log("debug", "Initializing client instance");

	this.parent = parent;
	this.io = io;
	this.client = client;

	this.register();
};

Pong5.Client.prototype = {

	"current_player": null,

	/**
	 * Register socket.io events
	 */
	"register": function() {
		var self = this;

		$.log("debug", "Registered socket events");

		this.client.on("pong5:auth", function(data, respond) {
			self.auth(data, respond);
		});

		this.client.on("pong5:move", function(data) {
			self.move(data);
		});
	},


	/**
	 * Execute auth event from a player
	 * 
	 * @param  {Object} data		Player data
	 * @param  {Function} respond	Respond function
	 */
	"auth": function(data, respond) {
		$.log("debug", "Received 'auth' event");

		if (data && data.player) {
			$.log("debug", "User: " + data.player);

			// Store current player ref
			this.current_player = data.player;

			// Broadcast to other player
			this.client.broadcast.emit("pong5:auth", data);

			// Respond to client
			respond({ "ok": true });
		} else {
			$.log("warn", "Failed auth, missing data");

			// Missing data send error
			respond({
				"ok": false,
				"err": "Missing 'player' param"
			});
		}
	},

	"move": function(data) {
		this.client.broadcast.emit("pong5:move", {
			"player": this.current_player,
			"dir": data.dir
		});
	}
};



// Initialize pong5 server
Pong5.init();
