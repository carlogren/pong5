(function(window, $) {
	"use strict";


	/**
	 * ======================================================
	 *
	 * Pong5 - pong like game in html5
	 *
	 * @author Carl OGREN
	 * @license http://en.wikipedia.org/wiki/Beerware Beerware license
	 *
	 * "THE BEER-WARE LICENSE" (Revision 42): Carl OGREN wrote this file.
	 * As long as you retain this notice you can do whatever you want with this stuff.
	 * If we meet some day, and you think this stuff is worth it, you can buy me a beer in return
	 *
	 * ======================================================
	 */
	
	


	/**
	 * Pong5 game constructor
	 * @param {object} options Game options
	 */
	var Pong5 = function Pong5(options) {
		var self = this;

		// Apply options to instance
		if (_.isObject(options)) { _.extend(this, options); }

		// Init createjs stage
		this.stage = new createjs.Stage(this.canvas_id);

		this.init_game_parts();

		createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
		createjs.Ticker.setFPS(this.fps);

		// Listen to createjs ticker
		createjs.Ticker.addEventListener("tick", function(e) {
			if (!e.paused) { self.update(e); }
		});

		this.pause(true);

		this.update();

		this.register();
	};

	Pong5.prototype = {
		// Canvas element's id
		"canvas_id": "game",

		// Createjs stage instance
		"stage": null,

		// Game ball instance
		"ball": null,
		"ball_defaults": {
			"x": 320,
			"y": 460,
			"dx": -0.7,
			"dy": -0.6,
			"speed": 300
		},

		"players": {
			"player1": {
				"defaults": {
					"x": 270,
					"y": 470
				},
				"name": "player1",
				"auth": false
			},
			"player2": {
				"defaults": {
					"x": 270,
					"y": 0
				},
				"name": "player2",
				"auth": false
			}
		},
		"selected_player": "",

		"player1": null,

		"player2": null,

		"fps": 30,


		/**
		 * Init game parts like the ball and the bats
		 */
		"init_game_parts": function() {
			// Initialize a ball
			// this.ball = new Pong5.Ball(_.extend({}, this.ball_defaults, { "game": this }));

			// Initialize one player
			this.player1 = new Pong5.Racket(_.extend({}, this.players.player1.defaults, { "game": this }));

			this.player2 = new Pong5.Racket(_.extend({}, this.players.player2.defaults, { "game": this }));
		},


		"pause": function(value) {
			if (_.isBoolean(value)) { createjs.Ticker.setPaused(value); }
		},

		"reset": function() {
			// Pause ticker
			this.pause(true);

			// Reset ball data
			// this.ball.reset(this.ball_defaults);

			// Reset player 1 data
			this.player1.reset(this.players.player1.defaults);

			// Update stage
			this.update();
		},


		/**
		 * Update the game for each frame
		 * @param  {createjs.Event} e createjs.Ticker event object
		 */
		"update": function(e) {
			if (e !== undefined) {
				// this.ball.update(e);
				this.player1.update(e);
				this.player2.update(e);
			}

			this.stage.update(e);
		},


		"register": function() {
			var self = this;

			Pong5.net.on("node:connect_failed", function(e) {
				alert("Could not connect to relay server");
			});

			Pong5.net.on("pong5:auth", function(e) {
				if (self.players[e.player].auth) {
					alert("Player already taken");
				} else {
					self.auth(e.player, false);
				}
			});


			Pong5.net.on("pong5:move", function(e) {
				self[e.player].direction = e.dir;
			});
		},


		"auth": function(player, local) {
			console.log("Send auth event");

			var self = this,
				exec = function(res) {
					self.players[player].auth = res.ok;
					self[player].setup(local);
				};

			if (local) {
				Pong5.net.auth(player, function(res) { exec(res); });
			} else {
				exec({ "ok": true });
			}
		},

		"ready_to_play": function() {
			return this.players.player1.auth && this.players.player2.auth;
		}
	};


	// Make the game object public
	window.Pong5 = Pong5;



	// Wait for window load
	$(function() {
		// Initialize game
		window.pong5 = new Pong5();

		$("#start-game").click(function(e) {
			e.preventDefault();

			window.pong5.pause(true);
			window.pong5.reset();
			window.pong5.pause(false);

			window.pong5.stage.canvas.focus();
		});

		$("#player1, #player2").click(function(e) {
			e.preventDefault();
			pong5.auth(this.id, true);
		});

		$("#connect").click(function(e) {
			e.preventDefault();
			// Connect to relay server
			Pong5.net.connect();
		});
	});


})(window, jQuery);