(function(window, Pong5) {
	"use strict";

	/**
	 * Pong5 ball constructor
	 * @param {Object} options An object defining the color, direction, speed and size of the ball
	 */
	Pong5.Ball = function Ball(options) {
		// Apply options to instance
		if (_.isObject(options)) { _.extend(this, options); }

		// Init graphics
		this.init_graphics();
	};

	Pong5.Ball.prototype = {
		"game": null,

		// Position variables
		"x": 0,
		"y": 0,

		// Direction variables, represented as x/y deltas
		"dx": -0.5,
		"dy": -0.5,

		// Movement variables
		"speed": 400,			// pixels/second
		"speed_increase": 10,	// Difficulty (kinda)

		// Display variables
		"color": "green",
		"radius": 3,

		// createjs graphics instance
		"gfx": null,
		// createjs shape instance
		"shape": null,


		/**
		 * Initialize createjs graphics, apply colors and create shape from it
		 */
		"init_graphics": function() {
			// Create ball graphics
			this.gfx = new createjs.Graphics();

			// Apply color and draw circle
			this.gfx.beginFill(this.color).drawCircle(0, 0, this.radius);

			// Create shape from graphics
			var shape = new createjs.Shape(this.gfx);

			// Apply position
			shape.x = this.x;
			shape.y = this.y;

			// Store the created shape
			this.shape = shape;

			// Add shape to stage
			this.game.stage.addChild(this.shape);
		},


		/**
		 * Detect collisions with borders and bats
		 */
		"detect_collisions": function() {
			if (this.x <= 0) {			// Left border
				this.dx = Math.abs(this.dx);
				this.x = 0;
			} else if (this.x >= 640) {	// Right border
				this.dx = -Math.abs(this.dx);
				this.x = 640;
			} else if (this.y <= 0) {	// Top border
				this.dy = Math.abs(this.dy);
				this.y = 0;
			} else if (this.y >= 480) {	// Bottom border
				console.log("Ball bottom border collision! you lost a life ...");

				this.game.reset();

				// @todo Handle score
			}
		},


		/**
		 * Update the ball position, angle, speed
		 */
		"update": function(e) {
			// Calculate time based speed
			var speed = e.delta / 1000 * this.speed;

			this.x += this.dx * speed;
			this.y += this.dy * speed;

			this.detect_collisions();

			this.shape.x = this.x;
			this.shape.y = this.y;
		},


		/**
		 * Reset the ball with the given data
		 * @param  {object} data The parameters to apply
		 */
		"reset": function(data) {
			_.extend(this, data);
		}
	};



})(window, Pong5);