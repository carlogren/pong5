(function(window, Pong5) {
	"use strict";


	/**
	 * Pong5 racket constructor
	 * @param {object} options Racket options
	 */
	Pong5.Racket = function Racket(options) {
		if (_.isObject(options)) { _.extend(this, options); }

		console.log("initialized racket");

		this.init_graphics();
	};

	Pong5.Racket.prototype = {
		"game": null,

		// Position and movement
		"x": 200,
		"y": 470,
		"direction": 0,		// -1=left, 0=stay, 1=right
		"speed": 250,

		// Size
		"width": 100,
		"height": 10,
		"radius": 3,

		// Display
		"color": "green",

		// create graphics and shape instances
		"gfx": null,
		"shape": null,

		// Currently pressed keyboard key
		"current_key": null,

		"is_current_player": true,


		/**
		 * Init racket graphics
		 */
		"init_graphics": function() {
			this.gfx = new createjs.Graphics();

			this.gfx.beginFill(this.color).drawRoundRect(0, 0, this.width, this.height, this.radius);

			this.shape = new createjs.Shape(this.gfx);

			this.shape.x = this.x;
			this.shape.y = this.y;

			this.game.stage.addChild(this.shape);
		},

		"setup": function(local) {
			console.log("setup " + (local ? "local" : "distant") + " player");
			if (local) {
				this.bind_keyboard();
				this.is_current_player = true;
			} else {
				this.is_current_player = false;
			}
		},


		/**
		 * Listen to keyboard events
		 */
		"bind_keyboard": function() {
			var self = this;

			console.log("binding keyboard");

			this.game.stage.canvas.addEventListener("keydown", function(e) {
				self.keydown(e);
			}, true);

			this.game.stage.canvas.addEventListener("keyup", function(e) {
				self.keyup(e);
			}, true);
		},


		/**
		 * Interpret a keydown event
		 * @param  {object} e Keyboard event object
		 */
		"keydown": function(e) {
			if (this.current_key == e.keyCode) { return; }

			if (e.keyCode == 37) {			// Left arrow
				this.current_key = e.keyCode;
				this.direction = -1;

				console.log("keydown, dir:" + this.direction);

				Pong5.net.move({ "dir": this.direction });
			} else if (e.keyCode == 39) {	// Right arrow
				this.current_key = e.keyCode;
				this.direction = 1;

				console.log("keydown, dir:" + this.direction);

				Pong5.net.move({ "dir": this.direction });
			}
		},


		/**
		 * Interpret a keyup event
		 * @param  {object} e Keyboard event object
		 */
		"keyup": function(e) {
			if (e.keyCode == 37 || e.keyCode == 39) {
				if (this.current_key == e.keyCode) {
					this.current_key = null;
					this.direction = 0;

					console.log("keyup, dir:" + this.direction);

					Pong5.net.move({ "dir": this.direction });
				}
			}
		},


		/**
		 * Detect collisions with borders and the ball
		 */
		"detect_collisions": function() {
			// Detect collisions with borders
			if (this.x <= 0) {							// Left border
				this.x = 0;
			} else if ((this.x + this.width) >= 640) {	// Right border
				this.x = 640 - this.width;
			}

			// Detect collisions with the ball
			var ball = this.game.ball;
			if ((ball.x >= this.x && ball.x <= (this.x + this.width)) && (ball.y >= this.y)) {
				console.log("Racket/Ball collision!");

				ball.speed += ball.speed_increase;

				// Determine ball position according to racket
				var ball_position = ((ball.x - this.x) / this.width) * 100;

				if (ball_position < 10) {
					ball.dx = -0.8;
					ball.dy = -0.1;
				} else if (ball_position < 20) {
					ball.dx = -0.6;
					ball.dy = -0.2;
				} else if (ball_position < 30) {
					ball.dx = -0.4;
					ball.dy = -0.4;
				} else if (ball_position < 40) {
					ball.dx = -0.2;
					ball.dy = -0.6;
				} else if (ball_position < 50) {
					ball.dx = -0.1;
					ball.dy = -0.8;
				} else if (ball_position < 60) {
					ball.dx = 0.1;
					ball.dy = -0.8;
				} else if (ball_position < 70) {
					ball.dx = 0.2;
					ball.dy = -0.6;
				} else if (ball_position < 80) {
					ball.dx = 0.4;
					ball.dy = -0.4;
				} else if (ball_position < 90) {
					ball.dx = 0.6;
					ball.dy = -0.2;
				} else if (ball_position <= 100) {
					ball.dx = 0.8;
					ball.dy = -0.1;
				}

				// ball.dy = -Math.abs(ball.dy);
				ball.y = this.y;

				// @todo Handle ball direction according to collision point between the racket and the ball
			}
		},


		/**
		 * Update racket display properties
		 * @param  {createjs.Ticker.Event} e Ticker event object
		 */
		"update": function(e) {
			// Calculate time based speed
			var speed = e.delta / 1000 * this.speed;

			this.x += this.direction * speed;

			// this.detect_collisions();

			this.shape.x = this.x;
		},


		/**
		 * Reset the racket with the given data
		 * @param  {object} data Parameters to apply
		 */
		"reset": function(data) { _.extend(this, data); }
	};




})(window, Pong5);