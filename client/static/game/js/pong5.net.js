(function(window, $) {
	"use strict";


	/**
	 * Pong5 Net handler
	 */
	Pong5.net = {
		// Socket.io configuration
		"config": {
			"port": 1234,
			"dns": "192.168.0.21"
		},

		"socket": null,

		"init": function() {
			_.extend(this, Backbone.Events);
		},

		"connect": function() {
			var self = this;

			// Connect socket.io client
			this.socket = io.connect(this.config.dns + ":" + this.config.port);

			this.socket.on('error', function(reason) {
				self.trigger('node:error', reason);
			});

			//Register callback for a successful connection
			this.socket.on('connect', function() {
				self.trigger('node:connect_success');
			});

			//Register callback for a successful reconnection
			this.socket.on('reconnect', function() {
				self.trigger('node:reconnect');
			});

			//Failed connection callback
			this.socket.on('connect_failed', function() {
				self.trigger('node:connect_failed');
			});

			//Disconnection from server callback
			this.socket.on('disconnect', function() {
				self.trigger('node:disconnect');
			});

			//Failed reconnection
			this.socket.on('reconnect_failed', function() {
				self.trigger('node:reconnect_failed');
			});

			// User auth-ed into the app
			this.socket.on("pong5:auth", function(data) {
				self.trigger("pong5:auth", data);
			});

			this.socket.on("pong5:move", function(data) {
				self.trigger("pong5:move", data);
			});
		},

		"auth": function(player, callback) {
			// Send pong5:auth event to server
			this.socket.emit("pong5:auth", {
				"player": player
			}, function(res) {
				if (_.isFunction(callback)) { callback(res); }
			});
		},

		"move": function(data) {
			this.socket.emit("pong5:move", data);
		}
	};

	// Initialize net lib
	Pong5.net.init();


})(window, jQuery);